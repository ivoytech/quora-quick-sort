# Q&AFilterChromeExtension
A filter of answers in Q&A based sites like for quora based on stats

Update 2.1 Address trademark infringement claims from Quora as of August 14th, 2019.

Update 2.0 Addressed copyright issue with Quora; changed extension icon and display as of July 8th, 2019.

Update 1.9.5.1 Fixed issue with Quora version update as of May 22nd, 2019 for sorting by upvotes due to element name change of upvote holder for posfixed text.

Update 1.9.5.0 Fixed issue with Quora version update as of May 16th, 2019 for sorting and preloading due to element location changes.

Update 1.9.4.0 Fixed issue with Quora version udpate as of April 27th, 2019 for upvote and views location changes.

Update 1.9.3.1 Fixed issue with Quora version update as of March 15th, 2019 for upvote class container change. ** note: disabled sort by comments until the functionality is fixed **

Update 1.9.3.0 Fixed issue with Quora version update as of March 5th, 2019 for answer list location change.

Update 1.9.2.0 Fixed issue with sorting in general as of Feb 16, 2019 for ads container placement.

Update 1.8.11.0 Fixed issue with Quora version update as of Nov 27th, 2018 for ads ranking and date parsing.

Update 1.8.10.0 Fixed issue with Quora version update as of Oct 17th, 2018 for upvote container changes.

Update 1.8.9.0 Fixed issue with Quora version update as of Sep 14th, 2018 for upvote element variation.

Update 1.8.8.0 Fixed issue with Quora version update as of Aug 29th, 2018 for ads issue.

Update 1.8.6.0 Fixed issue with Quora version update as of May 29th, 2018.

Update 1.8.5.0 Fixed issue with sorting ghost answers.

Update 1.8.4.0 Fixed issue with new Quora upvote interface.

Update 1.8.3.0 Fixed issue with recency sorting and loading progress display.

Update 1.8.2.0 Fixed issue with loading stuck at 99%. Increased preloading efficiency.

Update 1.8.1.0 Modified to support Quora version as of Jan 12th, 2018.

Update 1.7.7.0 Modified to support newest quora version as of Dec 9th, 2017.

Update 1.7.4.13 Supports preloading answers. Can now sort by date.

Update 1.7.4.12 Supports sorting existing answers by views and comments.

Update 1.0 Supports sorting existing quora answers that are already loaded into page by upvotes.

GOALS:

1. Support filtering by # of views. (Complete)

2. Support filtering by # of comments. (Complete)

3. Support preloading answers during filtering. (Complete)

4. Support filtering by dates. (Complete)

5. Support flexible filtering by intervals.

6. Support ending preloading functionality early.
 