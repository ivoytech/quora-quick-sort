function removeK(num){
	if (typeof num != "string"){
		return num
	}
	if (num.includes("k")){
		return parseFloat(num.replace("k",""))*1000;
	} else if (num.includes("m")){
		return parseFloat(num.replace("m",""))*1000000;
	} else {
		return parseFloat(num);
	}	
}
function removePlus(num){
	if (num==null){
		return 0;
	}
	if (num.includes("+")){
		return removeK(num.replace("+",""));
	} else {
		return removeK(num);
	}	
}


// Update 1: As of Oct 17, 2018, Quora added different elements to hold upvote count
// This function identifies if the answer's upvote is represented using which method
// If none of the existing two methods are used, the answer will be dropped to the bottom

// Update 2: Quora seemed to switched back to single class holding the upvote count
function getAnswerCount(answer) {
	var upvoteBar = $(answer).find('.ui_button_count:not(.hidden)')[0];
	if (upvoteBar == null) {
		return -1
	}
	if ($(upvoteBar).find('.ui_button_count_optimistic_count')[0] != null) {
		return $(upvoteBar).find('.ui_button_count_optimistic_count')[0].innerHTML;
	} else if ($(upvoteBar).find('.ui_button_count_inner')[0] != null){
		return $(upvoteBar).find('.ui_button_count_inner')[0].innerHTML;
	} else {
		return -1;
	}
}




var answerList = $(".paged_list_wrapper:not(.spinner_display_area)")[0];
var answerListOutter = $(".paged_list_wrapper:not(.spinner_display_area)").parent()[0];


//removed collapsed answers
var collapsedList;
if (typeof(answerList.getElementsByClassName('CollapsedAnswersSectionCollapsed')[0]) != 'undefined')
{
	collapsedList = answerList.getElementsByClassName('CollapsedAnswersSectionCollapsed')[0].parentNode.parentNode.parentNode;
	collapsedList.parentNode.removeChild(collapsedList);
}
//main

var answers = Array.prototype.slice.call(answerList.children,0);

//sorted by upvotes
var sortedList = answers.sort(function(a,b){
	if ((b.children.length == 0 || b.getElementsByClassName('ui_button_count')[0] == null)
		|| (a.getElementsByClassName('ui_button_count')[0] != null 
			&& b.getElementsByClassName('ui_button_count')[0] != null
			&& (removeK(getAnswerCount(a)) > removeK(getAnswerCount(b)))
		)
	){
		return -1;
	} else {
		return 1;
	}	
});

answerList.innerHTML = "";

//by upvotes: REMOVED DUE TO QUORA REMOVAL OF PAGE LIST HIDDEN ITEMS
 for (var i=0;i<sortedList.length;i++){
// 	if (i>7){
// 		sortedList[i].setAttribute('class', 'pagedlist_item pagedlist_hidden');
// 		sortedList[i].style.display='none';
// 	} else {
// 		sortedList[i].setAttribute('class', 'pagedlist_item');
// 		sortedList[i].removeAttribute('style');		
// 	}
 	answerList.appendChild(sortedList[i]);
 }

// removed promotiona list append because no longer applicable
// for (var i =0; i<promotionList.length;i++){
// 	var promotion = promotionList[i].parentNode.parentNode;
// 	answerList.appendChild(promotion);
// }
if (collapsedList!=null){
	answerList.appendChild(collapsedList);
}

// deprecated : no more more answer button in the fe
//answerListOutter.appendChild(moreAnsButton);

document.getElementsByClassName('QuestionPageAnswerHeader')[0].scrollIntoView( true );
window.scrollBy(0,-62);