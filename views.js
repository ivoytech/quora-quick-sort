function removeK(num){
	num = num.replace("views", "");
	num = num.replace("view", "");
	num = num.trim()
	if (num.includes("k")){
		return parseFloat(num.replace("k",""))*1000;
	} else if (num.includes("m")){
		return parseFloat(num.replace("m",""))*1000000;
	} else {
		return parseFloat(num);
	}	
}

var answerList = $(".paged_list_wrapper:not(.spinner_display_area)")[0];
var answerListOutter = $(".paged_list_wrapper:not(.spinner_display_area)").parent()[0];

// deprecated : no more more answer button in the fe
// var moreAnsButton = answerListOutter.getElementsByClassName('pager_next')[0];
// moreAnsButton.parentNode.removeChild(moreAnsButton);

// var promotionList = answerList.getElementsByClassName('answer_area_content');

// for (var i =0; i<promotionList.length;i++){
// 	var promotion = promotionList[i].parentNode.parentNode;
// 	promotion.parentNode.removeChild(promotion);
// }

//removed collapsed answers
var collapsedList;
if (typeof(answerList.getElementsByClassName('CollapsedAnswersSectionCollapsed')[0]) != 'undefined')
{
	collapsedList = answerList.getElementsByClassName('CollapsedAnswersSectionCollapsed')[0].parentNode.parentNode.parentNode;
	collapsedList.parentNode.removeChild(collapsedList);
}
//main
var answers = Array.prototype.slice.call(answerList.children,0);

var sortedListByViews = answers.sort(function(a,b){
	if ((b.children.length == 0 || b.getElementsByClassName('ContentFooter AnswerFooter ReadingContentFooter')[0] == null)
		|| (a.getElementsByClassName('ContentFooter AnswerFooter ReadingContentFooter')[0] != null 
			&& b.getElementsByClassName('ContentFooter AnswerFooter ReadingContentFooter')[0] != null
			&& (removeK(a.getElementsByClassName('ContentFooter AnswerFooter ReadingContentFooter')[0].children[0].innerHTML) > removeK(b.getElementsByClassName('ContentFooter AnswerFooter ReadingContentFooter')[0].children[0].innerHTML))
		)
	){
		return -1;
	} else {
		return 1;
	}	
});
answerList.innerHTML = "";

//by views: REMOVED DUE TO QUORA REMOVAL OF PAGE LIST HIDDEN ITEMS
 for (var i=0;i<sortedListByViews.length;i++){
// 	if (i>7){
// 		sortedListByViews[i].setAttribute('class', 'pagedlist_item pagedlist_hidden');
// 		sortedListByViews[i].style.display='none';
// 	} else {
// 		sortedListByViews[i].setAttribute('class', 'pagedlist_item');
// 		sortedListByViews[i].removeAttribute('style');
// 	}
 	answerList.appendChild(sortedListByViews[i]);
 }

// deprecated because no longer applicable
// for (var i =0; i<promotionList.length;i++){
// 	var promotion = promotionList[i].parentNode.parentNode;
// 	answerList.appendChild(promotion);
// }
if (collapsedList!=null){
	answerList.appendChild(collapsedList);
}
// deprecated : no more more answer button in the fe
// answerListOutter.appendChild(moreAnsButton);

document.getElementsByClassName('QuestionPageAnswerHeader')[0].scrollIntoView( true );
window.scrollBy(0,-62);
