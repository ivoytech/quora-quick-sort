function removeK(num){
	if (num.includes("k")){
		return parseFloat(num.replace("k",""))*1000;
	} else if (num.includes("m")){
		return parseFloat(num.replace("m",""))*1000000;
	} else {
		return parseFloat(num);
	}	
}

function removePlus(num){
	if (num==null){
		return 0;
	}
	if (num.includes("+")){
		return removeK(num.replace("+",""));
	} else {
		return removeK(num);
	}	
}

//calculates the number of comments using the non-featured comments container
function getCommentCountByAnswer (answer) {
	var checkExist = setInterval(function() {
		if (answer.getElementsByClassName('AnswerNonFeaturedTCommentListWrapper')[0] != undefined) {
		   clearInterval(checkExist);
		}
	 }, 100);
	
	if (answer.getElementsByClassName('AnswerNonFeaturedTCommentListWrapper')[0] == undefined) {
		return 0;
	}
	var commentList = answer.getElementsByClassName('AnswerNonFeaturedTCommentListWrapper')[0].children[0].children
	var listSize = commentList.length;
	var commentsCount = 0;
	const averageCommentSize = 25;
	const initialCommentSize = 5;
	//check if there's a section for collapsed comments
	var collpasedLink = commentList[listSize-1].id.indexOf('collapsed_link') > -1;
	if (collapsedLink) {
		listSize = listSize - 1;
	}

	//check if there's a section for more comments
	var commentsMoreButton = commentList[listSize-1].className.indexOf('comments_more_button') > -1;
	if (commentsMoreButton){
		listSize = listSize - 1;
	}

	//if there's more comments than the first five, make an estimate
	if (listSize > 2){
		commentsCount = (listSize-3)*averageCommentSize + initialCommentSize;
	}

	//Add the initial showing ones
	commentsCount = commmentsCount + commentList[1].length;
	return commentsCount;
}

function preToggleComments(answers){
	for (var i=0;i<answers.length;i++){
		answer = answers[i];
		var showAllComments = answer.getElementsByClassName('toggle_all')[0];
		var commentsPreview = answer.getElementsByClassName('comments_preview_toggle')[0];		
		if (showAllComments != undefined) {
			var commentDisabled = showAllComments.className.indexOf('is_disabled') > -1;
			if (!commentDisabled) {
				showAllComments.click();
			}
		} else if (commentsPreview != undefined) {
			commentsPreview.click();
		} else {

		}
	}
}

var answerList = $(".paged_list_wrapper")[0];
var answerListOutter = $(".paged_list_wrapper").parent()[0];

// deprecated : no more more answer button in the fe
// var moreAnsButton = answerListOutter.getElementsByClassName('pager_next')[0];
// moreAnsButton.parentNode.removeChild(moreAnsButton);

//remove promotions
var promotionList = answerList.getElementsByClassName('answer_area_content');

for (var i =0; i<promotionList.length;i++){
	var promotion = promotionList[i].parentNode.parentNode;
	promotion.parentNode.removeChild(promotion);
}
//removed collapsed answers
var collapsedList;
if (typeof(answerList.getElementsByClassName('CollapsedAnswersSectionCollapsed')[0]) != 'undefined')
{
	collapsedList = answerList.getElementsByClassName('CollapsedAnswersSectionCollapsed')[0].parentNode.parentNode.parentNode;
	collapsedList.parentNode.removeChild(collapsedList);
}
//main
var answers = Array.prototype.slice.call(answerList.children,0);

preToggleComments(answers);

//sorted by comments
var sortedListByComments = answers.sort(function(a,b){
	if (typeof(a.getElementsByClassName('count')[0]) != undefined && typeof(b.getElementsByClassName('count')[0]) != undefined){
		if (getCommentCountByAnswer(a) > getCommentCountByAnswer(b)){
			return -1;
		} else {
			return 1;
		}
	} else if (typeof(a.getElementsByClassName('count')[0]) == undefined) {
		return 1;
	} else if (typeof(b.getElementsByClassName('count')[0]) == undefined) {
		return -1;
	}
});

answerList.innerHTML = "";

//by comments: REMOVED DUE TO QUORA REMOVAL OF PAGE LIST HIDDEN ITEMS
 for (var i=0;i<sortedListByComments.length;i++){
// 	if (i>7){
// 		sortedListByComments[i].setAttribute('class', 'pagedlist_item pagedlist_hidden');
// 		sortedListByComments[i].style.display='none';
// 	} else {
// 		sortedListByComments[i].setAttribute('class', 'pagedlist_item');
// 		sortedListByComments[i].removeAttribute('style');
// 	}
 	answerList.appendChild(sortedListByComments[i]);
 }

//add promotions back
for (var i =0; i<promotionList.length;i++){
	var promotion = promotionList[i].parentNode.parentNode;
	answerList.appendChild(promotion);
}

if (collapsedList!=null){
	answerList.appendChild(collapsedList);
}
// deprecated : no more more answer button in the fe
// answerListOutter.appendChild(moreAnsButton);

document.getElementsByClassName('QuestionPageAnswerHeader')[0].scrollIntoView( true );
window.scrollBy(0,-62);
